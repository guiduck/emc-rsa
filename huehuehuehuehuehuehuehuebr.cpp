#include <stdio.h>
#include <stdlib.h>
#include <math.h>

long mod(long a, long b)
{
    long r = a % b;

    /* Uma corre��o � necess�ria se r e b n�o forem do mesmo sinal */

    /* se r for negativo e b positivo, precisa corrigir */
    if ((r < 0) && (b > 0))
	return (b + r);

    /* Se r for positivo e b negativo, nova corre��o */
    if ((r > 0) && (b < 0))
	return (b + r);

    return (r);
}

int char_int(char c) {
	
	char alfa[26] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
	int i;
	
	for(i = 0; i < 25; i++) {
		
		if(alfa[i] == c)
			return i;
			
	}
	
	return 0;
	
}

char int_char(int x) {
	
	char alfa[26] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
	return alfa[x];
	
}

long euclides_ext(long a, long b, long c)
{
    long r;

    r = mod(b, a);

    if (r == 0) {
	return (mod((c / a), (b / a)));	// retorna (c/a) % (b/a)
    }

    return ((euclides_ext(r, a, -c) * b + c) / (mod(a, b)));
}

int main()
{
    long p, q, e, qq, n, d, c, m;
    char cc, mm;
    FILE * arquivo, * resposta;
    arquivo = fopen("emc.txt", "r");
  	resposta = fopen("emcdois.txt", "w");
  	printf("Digite os valores de p, q e e: ");
  	scanf("%li %li %li", &p, &q, &e);
  	mm = fgetc(arquivo);
  	while(mm != EOF) {
  		
  		if (mm != ' ' && mm != '-' && mm != ',' && mm != '.') {
  		m = char_int(mm);
    	n = p * q;
	
   		qq = (p - 1) * (q - 1);

    	d = euclides_ext(e, qq, 1);
    	c = mod(pow(m, e), n);
    	if (c >= 26)
    		c = mod(c, 26);
    		
    	cc = int_char(c);
    	fprintf(resposta, "%c", cc);
    	
    	}
    	mm = fgetc(arquivo);
    	
	}
    	
    	fclose(arquivo);
	
		printf("Mensagem testada: %c\n\n", mm);
	    printf("\n\nVALORES CALCULADOS:\n\n");
	    printf("N   = %10li\nE   = %10li\nPhi = %10li\nD   = %10li\n\n", n, e, qq, d);
	    printf("Mensagem criptografada: %c", cc);
	    printf("\nc = %d", c);
	
	return 0;

    }

